package Entities;

public class User {
    private int id;
    private String name;
    private String email;
    private String gender;
    private String status;
    private String create_at;
    private String update_at;

    public User(){

    }

    public User(int id, String name, String email, String gender, String status, String create_at, String update_at){
        this.id = id;
        this.name = name;
        this.email = email;
        this.gender = gender;
        this.status = status;
        this.create_at = create_at;
        this.update_at = update_at;
    }

    public User (String name, String email, String gender, String status){
        this.name = name;
        this.email = email;
        this.gender = gender;
        this.status = status;
    }

    public User(int id, String name, String email, String status){
        this.id = id;
        this.name = name;
        this.email = email;
        this.status = status;
    }

    public User( String name, String email, String status){
        this.name = name;
        this.email = email;
        this.status = status;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreate_at() {
        return create_at;
    }

    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }

    public String getUpdate_at() {
        return update_at;
    }

    public void setUpdate_at(String update_at) {
        this.update_at = update_at;
    }
}
