package gorest.co.in;

import Entities.User;
import Service.*;
import dnl.utils.text.table.TextTable;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    static Scanner sc = new Scanner(System.in);
    private static String DB_URL = SQLDatabaseConnection.getDB_URL();
    private static String USER_NAME = SQLDatabaseConnection.getUSER_NAME();
    private static String PASSWORD = SQLDatabaseConnection.getPASSWORD();
    static Connection conn = SQLDatabaseConnection.getConnection(DB_URL, USER_NAME, PASSWORD);

    public static void menu() {
        System.out.println("\n---------------------MENU----------------------\n"
                + "1. Retrieve all user and store into DB\n" +
                "2. Retrieve 1 user and store into DB\n" +
                "3. Create user in gorest\n" +
                "4. Update user gorest\n" +
                "5. Delete user gorest\n" +
                "6. View user gorest\n");

    }
    public static int getInt(Scanner sc1) {
        int a;
        while (true) {
            try {
                sc1 = new Scanner(System.in);
                a = sc1.nextInt();
                if (a == 1 || a == 2 || a == 3 || a == 4 || a ==5 || a == 6) {
                    break;
                } else {
                    System.out.println("Invalid answer. Please enter your choice from 1 to 6: ");
                }
            } catch (Exception e) {
                System.out.println("Invalid data type. Please enter an integer");
            }
        }
        return a;
    }

    public static void closeConnection(){
        try {
            conn.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }


    public static void main(String[] args) {
        try {


            // create statement
            Statement stmt = conn.createStatement();
            User user = new User();

            int options;
            ResultSet rs = null;

            while (true) {
                menu();
                options = getInt(sc);
                List<User> users;
                int inputId;

                switch(options){
                    case 1:
                        users = Utils.retrieveAllUsers(conn, rs, stmt);
                        UserService.printUserList(users);

                        break;

                    case 2:
                        System.out.println("Enter user's id = ");
                        inputId = InputUtils.getInt();
                        user = Utils.retrieve1User(conn, rs,inputId);
                        if (user != null){
                            System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                            System.out.format("|%4s|%20s|%32s|%10s|%10s|%60s|%60s|", "id", "name", "email", "gender", "status", "created at", "updated at");
                            //System.out.println("| id |            name          |                        email                     |  gender  |  status  |                   created_at                  |                  updated_at                   |");
                            System.out.println("\n=========================================================================================================================================================================================================");
                            System.out.format("|%4d|%20s|%32s|%10s|%10s|%60s|%60s|", user.getId(), user.getName(), user.getEmail(), user.getGender(), user.getStatus(), user.getCreate_at(), user.getUpdate_at());
                            System.out.println("\n---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");

                        } else {
                            System.out.println("");
                        }

                        break;

                    case 3:
                        System.out.println("Enter name = ");
                        String newName = InputUtils.getString();
                        System.out.println("Enter email = ");
                        String newEmail = InputUtils.getString();
                        System.out.println("Enter gender = ");
                        String newGender = InputUtils.getString();
                        System.out.println("Enter status = ");
                        String newStatus = InputUtils.getString();

                        User userResponse = Request.createUserAPI(newName, newEmail, newGender, newStatus);
                        user = UserService.insertUserDB(conn, userResponse.getId(), userResponse.getName(), userResponse.getEmail(), userResponse.getGender(),
                                userResponse.getStatus(), userResponse.getCreate_at(), userResponse.getUpdate_at());
                        if (user != null){
                            System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                            System.out.println("| id |            name          |                        email                     |  gender  |  status  |                   created_at                  |                  updated_at                   |");
                            System.out.println("=========================================================================================================================================================================================================");
                            System.out.format("|%4d|%20s|%32s|%10s|%10s|%60s|%60s|", user.getId(), user.getName(), user.getEmail(), user.getGender(), user.getStatus(), user.getCreate_at(), user.getUpdate_at());
                            System.out.println("\n---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");

                        } else {
                            System.out.println("");
                        }
                        break;

                    case 4:
                        System.out.println("\n-----------------------------------------UPPDATE USER INFO-----------------------------------------");
                        System.out.println("Enter user id need to update: ");
                        inputId = InputUtils.getInt();

                        System.out.println("Enter new name = ");
                        String updateName = InputUtils.getString();
                        System.out.println("Enter new email = ");
                        String updateEmail = InputUtils.getString();
                        System.out.println("Enter new status = ");
                        String updateStattus = InputUtils.getString();

                        Request.updateUserAPI(inputId, updateName, updateEmail, updateStattus);

                        break;

                    case 5:
                        System.out.println("\n-----------------------------------------DELETE USER INFO-----------------------------------------");
                        System.out.println("Enter user id you want to delete: ");
                        inputId = InputUtils.getInt();
                        Request.deleteUser(inputId, conn);
                        break;

                    case 6:
                        users = UserService.getUserList(rs, stmt);
                        if (users == null){
                            System.out.println("There is no result");
                        } else {
                            UserService.printUserList(users);
                        }

                        break;


                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        sc.close();

        closeConnection();

    }


}
