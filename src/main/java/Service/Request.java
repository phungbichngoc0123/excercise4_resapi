package Service;

import Entities.User;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.xml.ws.Response;
import java.sql.Connection;
import java.util.*;

public class Request {
    static RestTemplate restTemplate = new RestTemplate();
    static final String URL_CREATE_USER = "https://gorest.co.in/public-api/users";
    static String authorization = "Bearer 9ae0bc0b7767b5131996460ed466263fae8d58fbe00e1d9c371603646742d044";

    public static String getResponse(String url) throws JSONException {
        // HttpHeaders
        HttpHeaders headers = new HttpHeaders();

        headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));
        // tra ve kieu JSON
        headers.setContentType(MediaType.APPLICATION_JSON);

        // HttpEntity<String>: To get result as String.
        HttpEntity<String> entity = new HttpEntity<String>(headers);

        // RestTemplate
        RestTemplate restTemplate = new RestTemplate();

        // sed request with method GET
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);

        //get reponse to as a String
        String result = response.getBody();

        return  result;
    }



    public static User createUserAPI(String newName, String newEmail, String newGender, String newStatus){
        String result;
        User user = null;

        //request header
        HttpHeaders  headers = new HttpHeaders();
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");
        headers.add("Authorization", authorization);

        // request body parameters
        Map<String, Object> map = new HashMap<>();
        map.put("name", newName);
        map.put("gender", newGender);
        map.put("email", newEmail);
        map.put("status", newStatus);

        // build the request
        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);

        // send POST request
        ResponseEntity<String> response = restTemplate.postForEntity(URL_CREATE_USER, entity, String.class);

        if (response.getStatusCode() == HttpStatus.CREATED) {
            System.out.println("Request Successful");
            System.out.println(response.getBody().toString());
            result = response.getBody().toString();
            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONObject data = (JSONObject) jsonObject.get("data");

                for (int i = 0; i < data.length(); i++){
                    Integer id = Integer.parseInt(data.getString("id"));
                    String name = data.getString("name");
                    String email = data.getString("email");
                    String gender = data.getString("gender");
                    String status = data.getString("status");
                    String createdAt = data.getString("created_at");
                    String updatedAt = data.getString("updated_at");

                    user = new User(id, name, email, gender, status, createdAt, updatedAt);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            System.out.println("Request Failed");
            System.out.println(response.getStatusCode());
            System.out.println(response.getBody().toString());
        }
        return user;

    }

    public static User updateUserAPI(int id, String updateName, String updateEmail, String updateStatus){

        String result;
        User user = null;

        //request header
        HttpHeaders  headers = new HttpHeaders();
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");
        headers.add("Authorization", authorization);

        // request body parameters
        Map<String, Object> map = new HashMap<>();
        map.put("name", updateName);
        map.put("email", updateEmail);
        map.put("status", updateStatus);

        // build the request
        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);

        // send POST request
        ResponseEntity<String> response = restTemplate.exchange(URL_CREATE_USER, HttpMethod.PUT, entity, String.class);

        if (response.getStatusCode() == HttpStatus.CREATED) {
            System.out.println("Request Successful");
            System.out.println(response.getBody().toString());
            result = response.getBody().toString();
            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONObject data = (JSONObject) jsonObject.get("data");

                for (int i = 0; i < data.length(); i++){
                    id = Integer.parseInt(data.getString("id"));
                    String name = data.getString("name");
                    String email = data.getString("email");
                    String gender = data.getString("gender");
                    String status = data.getString("status");
                    String createdAt = data.getString("created_at");
                    String updatedAt = data.getString("updated_at");

                    user = new User(id, name, email, gender, status, createdAt, updatedAt);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            System.out.println("Request Failed");
            System.out.println(response.getStatusCode());
            System.out.println(response.getBody().toString());
        }
        return user;
    }

    public static void deleteUser(int id, Connection conn){
        // Gửi yêu cầu với phương thức DELETE.

        String deleteUserURL = URL_CREATE_USER + "/" + id;

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");
        headers.add("Authorization", authorization);

        HttpEntity<?> request = new HttpEntity<Object>(headers);

        ResponseEntity responseEntity= restTemplate.exchange(deleteUserURL, HttpMethod.DELETE, request, String.class);

        if (responseEntity.getStatusCode().equals(HttpStatus.OK)){
            if (UserService.removeAnUser(conn, id)){
                System.out.println("Successfully deleted user #" + id);
            } else {
                System.out.println("delete in db failed");
            }

        }


    }
}
