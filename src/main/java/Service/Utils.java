package Service;

import Entities.User;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Utils {

    static final String URL_USERS = "https://gorest.co.in/public-api/users";

    public static List<User> retrieveAllUsers(Connection conn, ResultSet rs, Statement stmt) {

        List<User> users = new ArrayList<User>();
        boolean isInsert = false;
        User u;
        String urlPage;

        try {
            String result = Request.getResponse(URL_USERS);
            JSONObject jsonObject = new JSONObject(result); //parse result to object

            if (jsonObject.getString("code").equals("200")){

                // get page num
                JSONObject meta = (JSONObject) jsonObject.get("meta");
                JSONObject pagination = (JSONObject) meta.get("pagination");

                int pageNum = Integer.parseInt(pagination.getString("pages"));
                System.out.println(pageNum);
                for (int i =1; i <= pageNum; i++){
                    urlPage = URL_USERS + "?page=" + i;
                    String response = Request.getResponse(urlPage);
                    JSONObject resultPage = new JSONObject(response);
                    JSONArray data = (JSONArray) resultPage.get("data"); // get array "Data" in object

                    //UserService.deleteAllUserInDb(conn);

                    for(int j = 0; j < data.length(); j++){
                        try {
                            Integer id = Integer.parseInt(data.getJSONObject(j).getString("id"));
                            String name = data.getJSONObject(j).getString("name");
                            String email = data.getJSONObject(j).getString("email");
                            String gender = data.getJSONObject(j).getString("gender");
                            String status = data.getJSONObject(j).getString("status");
                            String createdAt = data.getJSONObject(j).getString("created_at");
                            String updatedAt = data.getJSONObject(j).getString("updated_at");

                            boolean isExisted = UserService.isExisted(rs, conn, id);

                            if(isExisted){
                                u = UserService.updateUser(conn, rs, id, name, email, status, updatedAt);
                                users.add(u);
                            } else {
                                u = UserService.insertUserDB(conn, id, name, email, gender, status, createdAt, updatedAt);
                                users.add(u);
                            }

                        } catch (Exception e){
                            e.printStackTrace();
                        }

                    }

                    //users = UserService.getUserList(rs, stmt);
                }



            } else {
                System.out.println("Not found response");
                users = null;
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return users;

    }

    public static User retrieve1User(Connection conn, ResultSet rs, int idInput) {
        User u = null;
        String url = URL_USERS + "/" + idInput;
        boolean isInsert = false;

        try {
            String result = Request.getResponse(url);
            System.out.println(result);
            JSONObject jsonObject = new JSONObject(result); //parse result to object

            if(jsonObject.getString("code").equals("200")){
                JSONObject object1 = jsonObject.getJSONObject("data"); // get array "Data" in object

                // neu da ton tai thi ghi de
                UserService.removeAnUser(conn, idInput);

                try {
                    Integer id = Integer.parseInt(object1.getString("id"));
                    String name = object1.getString("name");
                    String email = object1.getString("email");
                    String gender = object1.getString("gender");
                    String status = object1.getString("status");
                    String createdAt = object1.getString("created_at");
                    String updatedAt = object1.getString("updated_at");

                    boolean isExisted = UserService.isExisted(rs, conn, id);

                    if(isExisted){
                        u = UserService.updateUser(conn, rs, id, name, email, status, updatedAt);
                    } else {
                        u = UserService.insertUserDB(conn, id, name, email, gender, status, createdAt, updatedAt);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }

            } else {
                System.out.println("Not found user");
                u = null;
            }

        } catch (JSONException  e) {
            e.printStackTrace();
        }
        return u;

    }

    public static void updateUser(int id, String name, String email, String status){
       // Request.UpdateUser(id, name, email, status);
    }


}
