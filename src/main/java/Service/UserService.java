package Service;

import Entities.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class UserService {

    public static boolean deleteAllUserInDb(Connection conn){
        int rowNum = 0;
        try {
            PreparedStatement ps = conn.prepareStatement("delete from users");
            rowNum = ps.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        if (rowNum > 0){
            return true;
        } else {
            return false;
        }
    }

    public static boolean isExisted(ResultSet rs, Connection conn, int id) throws SQLException {

        try {
            PreparedStatement ps = conn.prepareStatement("select * from users where id = ?");
            ps.setInt(1, id);
            rs = ps.executeQuery();


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return rs.next();

    }

    public static User updateUser(Connection conn,ResultSet rs, int id, String name, String email, String status, String updated_at) {

        int rowNum = 0;
        User u = null;

        String sql = "update users set name = ?, email = ?, status = ?, updated_at = ? where id = ?";

        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, name);
            ps.setString(2, email);
            ps.setString(3, status);
            ps.setString(4, updated_at);
            ps.setInt(5, id);

            rowNum = ps.executeUpdate();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (rowNum > 0){
            u = getSpecificAccount(rs, conn, id);
        } else {
            System.out.println("Update db failed");
        }
        return u;

    }


    public static User getSpecificAccount(ResultSet rs, Connection conn, int id) {
        User account = null;

        try {
            PreparedStatement ps = conn.prepareStatement("select * from users where id = ?");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            // show data

            while (rs.next()) {
                account = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
                        rs.getString(5), rs.getString(6), rs.getString(7));
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            System.out.println("There is no result");
        }

        return account;

    }

    public static boolean removeAnUser(Connection conn, int deleteUserId) {
        String sql = "delete from users where id = ?";

        int rowNum = 0;

        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, deleteUserId);
            rowNum = ps.executeUpdate();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (rowNum > 0){
            return true;
        } else {
            return false;
        }

    }

    public static User insertUserDB(Connection conn, int id, String name, String email, String gender, String status, String createAt, String updateAt) {

        int rowNum = 0;
        User u = null;

        String sql = "insert into users (id, name, email, gender, status, created_at, updated_at) values (?, ?, ?, ?, ?, ?, ?)";

        try {
            PreparedStatement ps = conn.prepareStatement(sql);

            //set param values
            ps.setInt(1, id);
            ps.setString(2, name);
            ps.setString(3, email);
            ps.setString(4, gender);
            ps.setString(5, status);
            ps.setString(6, createAt);
            ps.setString(7, updateAt);

            rowNum = ps.executeUpdate();
            //execute query

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            System.out.println("Unsuccessful insert user! Please try again");
            e.printStackTrace();
        }

        if (rowNum >=1 ) {
            u = new User(id, name, email, gender, status, createAt, updateAt);
        } else {
            System.out.println("Insert failed");
        }

        return u;
    }

    public static ArrayList<User> getUserList(ResultSet rs, Statement stmt) {
        ArrayList<User> users = new ArrayList<User>();

        try {
            rs = stmt.executeQuery("select * from users order by id");

            while (rs.next()) {

                users.add(new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
                        rs.getString(5), rs.getString(6), rs.getString(7)));

            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return users;
    }

    public static void printUserList(List<User> users){
        System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.format("|%4s|%20s|%32s|%10s|%10s|%60s|%60s|", "id", "name", "email", "gender", "status", "created at", "updated at");
        System.out.println("\n=========================================================================================================================================================================================================");

        for (User u : users) {
            System.out.format("|%4d|%32s|%60s|%10s|%10s|%60s|%60s|", u.getId(), u.getName(), u.getEmail(), u.getGender(), u.getStatus(), u.getCreate_at(), u.getUpdate_at());
            System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        }
    }

}
